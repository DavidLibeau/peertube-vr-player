function loadVideo(id, quality = "auto", timecode = 0, instanceTarget=instance) {
    console.log("LoadVideo:" + "https://" + instanceTarget + "/videos/watch/" + id + " " + quality + " " + timecode);
    ajaxGet("https://" + instanceTarget + "/api/v1/videos/" + id, function () {
        if (this.readyState == 4 && this.status == 200) {
            var videoInfo = JSON.parse(this.responseText);
            console.log(videoInfo);
            document.getElementById("videoControls").components["video-controls"].video_id=videoInfo.uuid;
            for (file in videoInfo.files) {
                if (quality == videoInfo.files[file].resolution.label) {
                    console.log(videoInfo.files[file].fileUrl);
                    document.getElementById("video").setAttribute("src", videoInfo.files[file].fileUrl);
                    document.getElementById("videoControls").setAttribute("video-controls", {
                        timecode: timecode,
                        quality: quality
                    });
                    document.getElementById("videoControls").emit("changeSource");
                    document.querySelector("a-sky").setAttribute("visible", "false");
                }
            }
            if(quality=="auto"){
                console.log(videoInfo.files[0].fileUrl);
                document.getElementById("video").setAttribute("src", videoInfo.files[0].fileUrl);
                document.getElementById("videoControls").setAttribute("video-controls", {
                    timecode: timecode,
                    quality: videoInfo.files[0].resolution.label
                });
                document.getElementById("videoControls").emit("changeSource");
            }
            
            //videosphere or videoplane
            var video360tags=new Array("360 video","video 360","vr video","video vr","360° video","video 360°","360 vidéo","vidéo 360","vr vidéo","vidéo vr","360° vidéo","vidéo 360°", "360video","video360","vrvideo","videovr","360°video","video360°","360vidéo","vidéo360","vrvidéo","vidéovr","360°vidéo","vidéo360°", "360-video","video-360","vr-video","video-vr","360°-video","video-360°","360-vidéo","vidéo-360","vr-vidéo","vidéo-vr","360°-vidéo","vidéo-360°");
            var isContainingVideo360tag=false;
            for (tag in videoInfo.tags) {
                if(video360tags.includes(videoInfo.tags[tag])){
                    isContainingVideo360tag=true;
                } 
            }
            if(isContainingVideo360tag){
                //var videoEl=document.getElementById("videosphereSrc");
                setVisible(document.getElementById("videosphere"),true);
                setVisible(document.getElementById("videoplane"),false);
                document.querySelector("a-sky").setAttribute("visible", "false");
            }else{
                //var videoEl=document.getElementById("videoplaneSrc");
                setVisible(document.getElementById("videosphere"),false);
                setVisible(document.getElementById("videoplane"),true);
                document.querySelector("a-sky").setAttribute("visible", "true");
            }
            
            if(document.getElementById("keyboard").components.keyboard.keyboardToggled){
                document.getElementById("keyboardToggle").emit("click");
            }
        }
    });
}

function searchVideo(q, instanceTarget=instance) {
    console.log("SeachVideo:" + "https://" + instanceTarget + "/api/v1/search/videos?search=" + q);
    if(q!=""){
        ajaxGet("https://" + instanceTarget + "/api/v1/search/videos?search=" + q, function () {
            if (this.readyState == 4 && this.status == 200) {
                var responseJson = JSON.parse(this.responseText);
                var resultEl = $(".videoResult");
                console.log(responseJson);
                for (var i = 0; i < resultEl.length; i++) {
                    if (responseJson.data[i]) {
                        resultEl.get(i).setAttribute("visible", "true");
                        $(resultEl.get(i)).attr("data-peertubevideo-id",responseJson.data[i].uuid);
                        $(resultEl.get(i)).children(".title").attr("value", responseJson.data[i].name);
                        $(resultEl.get(i)).children(".description").attr("value", responseJson.data[i].description);
                        $(resultEl.get(i)).children(".user").attr("value", "@" + responseJson.data[i].account.name + "@" + responseJson.data[i].account.host);
                        $(resultEl.get(i)).children(".thumbnail").attr("src", "https://" + instanceTarget + responseJson.data[i].thumbnailPath);
                    }else{
                        resultEl.get(i).setAttribute("visible", "false");
                    }
                }
            }
        });
    }else{
        $(".videoResult").hide();
    }
}

function showTrendingVideo(selector, instanceTarget=instance) {
    ajaxGet("https://" + instanceTarget + "/api/v1/videos/?sort=-views", function () {
            if (this.readyState == 4 && this.status == 200) {
                var responseJson = JSON.parse(this.responseText);
                var resultEl = $(selector);
                console.log(responseJson);
                for (var i = 0; i < resultEl.length; i++) {
                    if (responseJson.data[i]) {
                        resultEl.get(i).setAttribute("visible", "true");
                        $(resultEl.get(i)).attr("data-peertubevideo-id",responseJson.data[i].uuid);
                        $(resultEl.get(i)).children(".title").attr("value", responseJson.data[i].name);
                        $(resultEl.get(i)).children(".description").attr("value", responseJson.data[i].description);
                        $(resultEl.get(i)).children(".user").attr("value", "@" + responseJson.data[i].account.name + "@" + responseJson.data[i].account.host);
                        $(resultEl.get(i)).children(".thumbnail").attr("src", "https://" + instanceTarget + responseJson.data[i].thumbnailPath);
                        $(resultEl.get(i)).find(".duration").attr("value", createDurationString(responseJson.data[i].duration));
                        $(resultEl.get(i)).find(".views").attr("value", responseJson.data[i].views);
                    }else{
                        resultEl.get(i).setAttribute("visible", "false");
                    }
                }
            }
        });
}

$("[data-peertubevideo-id]").click(function(){
   console.log($(this).attr("data-peertubevideo-id")); 
});

$(document).ready(function(){
    $("[data-peertubevideo-id]").click(function(){
        loadVideo($(this).attr("data-peertubevideo-id"));
    });
});

function ajaxGet(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = callback;
    xhttp.open("GET", url, true);
    //xhttp.open("GET", "getCurl.php?q="+url, true);
    xhttp.send();
}

function createDurationString(duration) { //from Chocobozzz/PeerTube/client/src/app/shared/video/video.model.ts
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration % 3600) / 60);
    const seconds = duration % 60;

    const minutesPadding = minutes >= 10 ? '' : '0';
    const secondsPadding = seconds >= 10 ? '' : '0';
    const displayedHours = hours > 0 ? hours.toString() + ':' : '';

    return displayedHours + minutesPadding + minutes.toString() + ':' + secondsPadding + seconds.toString();
}

function setVisible(el, value){
    //console.log(el);
    //console.log(el.object3D.position.y);
    if(el.getAttribute("visible")!=value){
        el.setAttribute("visible", value);
        el.object3D.position.set(el.object3D.position.x, (value ? el.object3D.position.y-10 : el.object3D.position.y+10), el.object3D.position.z);
    }
    //console.log(el.getAttribute("visible"));
    //console.log(el.object3D.position.y);
}

var instance = "videonaute.fr";
/*setTimeout(function () {
    loadVideo("87b8af73-ec0a-4b3b-8fb5-eff0f1b82061", "1080p")
}, 5000);*/
showTrendingVideo(".videoHome");