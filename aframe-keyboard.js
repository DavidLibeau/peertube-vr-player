/******/
(function (modules) { // webpackBootstrap
    /******/ // The module cache
    /******/
    var installedModules = {};

    /******/ // The require function
    /******/
    function __webpack_require__(moduleId) {

        /******/ // Check if module is in cache
        /******/
        if (installedModules[moduleId])
            /******/
            return installedModules[moduleId].exports;

        /******/ // Create a new module (and put it into the cache)
        /******/
        var module = installedModules[moduleId] = {
            /******/
            exports: {},
            /******/
            id: moduleId,
            /******/
            loaded: false
            /******/
        };

        /******/ // Execute the module function
        /******/
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

        /******/ // Flag the module as loaded
        /******/
        module.loaded = true;

        /******/ // Return the exports of the module
        /******/
        return module.exports;
        /******/
    }


    /******/ // expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;

    /******/ // expose the module cache
    /******/
    __webpack_require__.c = installedModules;

    /******/ // __webpack_public_path__
    /******/
    __webpack_require__.p = "";

    /******/ // Load entry module and return exports
    /******/
    return __webpack_require__(0);
    /******/
})
/************************************************************************/
/******/
([
/* 0 */
/***/
    function (module, exports) {

        if (typeof AFRAME === "undefined") {
            throw new Error('Component attempted to register before AFRAME was available.');
        }

        /**
         ** Video control component for A-Frame.
         */

        AFRAME.registerComponent("keyboard", {
            schema: {},
            
            toUpperCase: function(){
                this.btns=document.querySelectorAll("[keyboard] [btn]");
                for (var i = 0; i < this.btns.length; i++) {
                    if(this.btns[i].getAttribute("key-value")!=" " && this.btns[i].getAttribute("key-value")!="maj" && this.btns[i].getAttribute("key-value")!="capslock" && this.btns[i].getAttribute("key-value")!="enter"){
                        this.btns[i].setAttribute("key-value",this.btns[i].getAttribute("key-value").toUpperCase());
                        if(this.btns[i].children[0].nodeName=="A-TEXT"){
                            this.btns[i].children[0].setAttribute("value",this.btns[i].children[0].getAttribute("value").toUpperCase());
                        }
                    }
                }
            },
            toLowerCase: function(){
                this.btns=document.querySelectorAll("[keyboard] [btn]");
                for (var i = 0; i < this.btns.length; i++) {
                    if(this.btns[i].getAttribute("key-value")!=" " && this.btns[i].getAttribute("key-value")!="maj" && this.btns[i].getAttribute("key-value")!="capslock" && this.btns[i].getAttribute("key-value")!="enter"){
                        this.btns[i].setAttribute("key-value",this.btns[i].getAttribute("key-value").toLowerCase());
                        if(this.btns[i].children[0].nodeName=="A-TEXT"){
                            this.btns[i].children[0].setAttribute("value",this.btns[i].children[0].getAttribute("value").toLowerCase());
                        }
                    }
                }
            },

            /**
             * Called once when component is attached. Generally for initial setup.
             */
            init: function () {
                var self=this;
                
                this.value="";
                this.isUpperCase=false;
                this.isCapsLock=false;
                
                this.keyboardToggled=false;
                
                document.getElementById("keyboardToggle").addEventListener("click",function(){
                    self.keyboardToggled=!self.keyboardToggled;
                    setVisible(self.el,!self.el.getAttribute("visible"));
                    document.querySelector("#videoControls>.controls").setAttribute("visible",!document.querySelector("#videoControls>.controls").getAttribute("visible"));
                    if(self.keyboardToggled){
                        document.getElementById("desk").object3D.position.set(0, 0.971, -0.671);
                        document.getElementById("desk").setAttribute("height",0.38);
                        document.querySelector("#desk>a-cylinder").object3D.position.set(0, -0.19, 0);
                    }else{
                        document.getElementById("desk").object3D.position.set(0, 1, -0.7);
                        document.getElementById("desk").setAttribute("height",0.3);
                        document.querySelector("#desk>a-cylinder").object3D.position.set(0, -0.15, 0);
                    }
                });
                
                this.btns=document.querySelectorAll("[keyboard] [btn]");
                for (var i = 0; i < this.btns.length; i++) {
                    this.btns[i].addEventListener("click", function (event) {
                        if(this.object3D.visible){
                            switch(this.getAttribute("key-value")){
                                case "maj" :
                                    if(self.isUpperCase){
                                        self.toLowerCase();
                                    }else{
                                        self.toUpperCase();
                                    }
                                    self.isUpperCase=!self.isUpperCase;
                                    self.isCapsLock=false;
                                    break;
                                case "capslock" :
                                    if(self.isUpperCase){
                                        self.toLowerCase();
                                    }else{
                                        self.toUpperCase();
                                    }
                                    self.isUpperCase=!self.isUpperCase;
                                    self.isCapsLock=!self.isCapsLock;
                                    break;
                                case "suppr" :
                                    self.value=self.value.substring(0, self.value.length - 1);
                                    console.log(self.value);
                                    document.getElementById("searchInput").setAttribute("value",self.value);
                                    break;
                                case "enter" :
                                    console.log("Enter : "+self.value);
                                    searchVideo(self.value);
                                    break;
                                default:
                                    console.log(this.getAttribute("key-value"));
                                    self.value+=this.getAttribute("key-value");
                                    console.log(self.value);
                                    document.getElementById("searchInput").setAttribute("value",self.value);
                                    if(self.isUpperCase && !self.isCapsLock){
                                        self.toLowerCase();
                                        self.isUpperCase=false;
                                    }
                                    break;
                            }
                        }
                        
                    });
                }
                
                

            },

            /**
             * Called when component is attached and when component data changes.
             * Generally modifies the entity based on the data.
             */
            update: function (oldData) {},

            /**
             * Called when a component is removed (e.g., via removeAttribute).
             * Generally undoes all modifications to the entity.
             */
            remove: function () {},

            /**
             * Called on each scene tick.
             */
            tick: function (t) {},
        });


        /***/
    }
/******/
    ]);