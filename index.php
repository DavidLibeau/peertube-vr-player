<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PeerTube VR – 360 video player</title>
    <meta name="description" content="PeerTube VR – 360 video player">
    <link rel="icon" type="image/png" href="https://videonaute.fr/client/assets/images/favicon.png" />
    <meta name="theme-color" content="#003141">
    <script src="https://aframe.io/releases/0.8.2/aframe.min.js"></script>
    <!-- Internal components -->
    <script src="aframe-video-controls/aframe-video-controls-modified.js"></script>
    <script src="aframe-btn.js"></script>
    <script src="aframe-keyboard.js"></script>
    <!-- External components -->
    <script src="aframe-video-illumination/aframe-video-illumination-component.min.js"></script>
    <!-- from: https://github.com/alfa256/aframe-video-illumination-component (AGPL-3.0) -->
    <script src="https://dav.li/jquery/3.1.1.min.js"></script>
    <script src="script.js"></script>
</head>

<body>
    <?php 
    $black="#0f0f0f";
    $grey="#00222a";
    ?>
    <a-scene>
        <a-assets>
            <img id="background" src="assets/images/Nasa-PIA15482.jpg" />
            <video id="video" src="" crossorigin="anonymous"></video>
            <img id="play-image" src="assets/icons/play.png" />
            <img id="pause-image" src="assets/icons/pause.png">
            <img id="backward-image" src="assets/icons/backward.png" />
            <img id="forward-image" src="assets/icons/forward.png" />
            <img id="beginning-image" src="assets/icons/beginning.png" />
            <img id="volumeOn-image" src="assets/icons/volume-on.png" />
            <img id="volumeOff-image" src="assets/icons/volume-off.png" />
            <img id="fullscreen-image" src="assets/icons/fullscreen.png" />
        </a-assets>

        <a-entity laser-controls="hand: right"></a-entity>
        <a-camera position="0 1.7 0" controls wasd-controls="enabled: false" cursor="rayOrigin: mouse"></a-camera>

        <a-videosphere id="videosphere" src="#video" rotation="0 180 0" material="side: back" position="0 10 0" visible="false"></a-videosphere>
        <a-video id="videoplane" src="#video" video-illumination="intensity: 0.5" geometry="height:0.99;width:1.76" position="0 11.6 -1.53" visible="false"></a-video>

        <a-entity id="videoControls" video-controls="src:#video;" visible="true">
            <a-entity class="controls">
                <!-- Btns -->
                <a-box class="playBtn" btn position="0 1.04 -0.62" rotation="-45 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.04;width:0.2;height:0.2">
                    <a-image src="#play-image" geometry="height:0.09;width:0.09"></a-image>
                </a-box>
                <a-cylinder class="backwardBtn" btn position="-0.2 1.04 -0.62" rotation="45 0 0" opacity="0.1" material="transparent:true" geometry="height:0.03;radius:0.07">
                    <a-image src="#backward-image" geometry="height:0.06;width:0.06" rotation="-90 0 0"></a-image>
                </a-cylinder>
                <a-cylinder class="forwardBtn" btn position="0.2 1.04 -0.62" rotation="45 0 0" opacity="0.1" material="transparent:true" geometry="height:0.03;radius:0.07">
                    <a-image src="#forward-image" geometry="height:0.06;width:0.06" rotation="-90 0 0"></a-image>
                </a-cylinder>
                <a-box class="beginningBtn" btn position="-0.37 1.04 -0.62" rotation="-45 0 0" opacity="0.1" material="transparent:true" geometry="width:0.14;height:0.14;depth:0.02">
                    <a-image src="#beginning-image" geometry="width:0.06;height:0.06"></a-image>
                </a-box>
                <a-box class="qualityBtn" btn position="0.37 1.04 -0.62" rotation="-45 0 0" opacity="0.1" material="transparent:true" geometry="width:0.14;height:0.14;depth:0.02">
                    <a-text value="auto" text="width:0.8;align:center"></a-text>
                    <a-entity class="btnMenu" visible="false" position="0 0.137 0.053" rotation="45 0 0">
                        <a-box class="changeQualityBtn" btn position="0 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="240p" text="align:center;width:0.8"></a-text>
                        </a-box>
                        <a-box class="changeQualityBtn" btn position="0 0.15 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="360p" text="align:center;width:0.8"></a-text>
                        </a-box>
                        <a-box class="changeQualityBtn" btn position="0 0.30 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="480p" text="align:center;width:0.8"></a-text>
                        </a-box>
                        <a-box class="changeQualityBtn" btn position="0 0.45 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="720p" text="align:center;width:0.8"></a-text>
                        </a-box>
                        <a-box class="changeQualityBtn" btn position="0 0.60 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="1080p" text="align:center;width:0.8"></a-text>
                        </a-box>
                        <a-box class="changeQualityBtn" btn position="0 0.75 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                            <a-text value="2160p" text="align:center;width:0.8"></a-text>
                        </a-box>
                    </a-entity>
                </a-box>
                <a-box class="volumeBtn" btn position="-0.541 1.04 -0.62" rotation="-45 0 0" opacity="0.1" material="transparent:true" geometry="width:0.14;height:0.14;depth:0.02">
                    <a-image src="#volumeOn-image" geometry="width:0.06;height:0.06"></a-image>
                </a-box>
                <a-box class="fullscreenBtn" btn position="0.541 1.04 -0.62" rotation="-45 0 0" opacity="0.1" material="transparent:true" geometry="width:0.14;height:0.14;depth:0.02">
                    <a-image src="#fullscreen-image" geometry="width:0.06;height:0.06"></a-image>
                </a-box>

                <!-- Time -->
                <a-text class="timeTotal" position="0.74 1.126 -0.714" rotation="-45 0 0" value="00:00" text="width:1;align:right"></a-text>
                <a-text class="timePlayed" position="-0.74 1.126 -0.714" rotation="-45 0 0" value="00:00" text="width:1;align:left"></a-text>
            </a-entity>

            <!-- Progress bar -->
            <a-entity class="progressBar" width="1.6" position="0 1.092 -0.795">
                <a-box material="emissive:#006d90;emissiveIntensity:0.4" geometry="depth: 0.05; height: 0.1" width="0.001" color="#006d90"></a-box>
                <!-- Played bar -->
                <a-box geometry="depth: 0.05; height: 0.1" width="0.001" color="#003141"></a-box>
                <!-- Buffered bar -->
                <a-box geometry="depth: 0.05; height: 0.1" width="1.6" color="black"></a-box>
                <!-- Background bar -->
            </a-entity>
        </a-entity>

        <!-- Keyboard -->
        <a-entity keyboard id="keyboard" position="-0.6 11.103 -0.688" rotation="-45 0 0" visible="false">
            <?php
            $firstLine=str_split("azertyuiop");
            $secondLine=str_split("qsdfghjklm");
            $thirdLine=str_split("wxcvbn?.@");
            $topLine=str_split("1234567890");
            $i=0;
            foreach($firstLine as $letter){
                echo('
                <a-box btn id="keyboardBtn-'.$letter.'" position="'.(0.12*$i).' 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.1" key-type="text" key-value="'.$letter.'">
                    <a-text value="'.$letter.'" text="align:center;width:0.8"></a-text>
                </a-box>');
                $i++;
            }
            $i=0;
            foreach($secondLine as $letter){
                echo('
                <a-box btn id="keyboardBtn-'.$letter.'" position="'.(0.05+0.12*$i).' -0.12 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.1" key-type="text" key-value="'.$letter.'">
                    <a-text value="'.$letter.'" text="align:center;width:0.8"></a-text>
                </a-box>');
                $i++;
            }
            $i=0;
            foreach($thirdLine as $letter){
                echo('
                <a-box btn id="keyboardBtn-'.$letter.'" position="'.(0.12+0.12*$i).' -0.24 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.1" key-type="text" key-value="'.$letter.'">
                    <a-text value="'.$letter.'" text="align:center;width:0.8"></a-text>
                </a-box>');
                $i++;
            }
            $i=0;
            foreach($topLine as $letter){
                echo('
                <a-box btn id="keyboardBtn-'.$letter.'" position="'.(0.12*$i).' 0.115 0.025" rotation="22.5 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.1" key-type="text" key-value="'.$letter.'">
                    <a-text value="'.$letter.'" text="align:center;width:0.8"></a-text>
                </a-box>');
                $i++;
            }
            ?>
                <a-box btn id="keyboardBtn-capslock" position="-0.035 -0.12 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.03" key-type="text" key-value="capslock">
                    <a-text value="m" text="align:center;width:0.8"></a-text>
                </a-box>
                <a-box btn id="keyboardBtn-maj-left" position="0 -0.24 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.1" key-type="text" key-value="maj">
                    <a-text value="maj" text="align:center;width:0.8"></a-text>
                </a-box>
                <a-box btn="" id="keyboardBtn-maj-right" position="1.232 -0.24 0" opacity="0.1" material="transparent:true" geometry="width:0.165;depth:0.02;height:0.1" key-type="text" key-value="maj" data-vivaldi-spatnav-clickable="1">
                    <a-text value="maj" text="align:center;width:0.8"></a-text>
                </a-box>
                <a-box btn id="keyboardBtn-space" position="0.54 -0.361 -0.034" rotation="22.5 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.47" key-type="text" key-value=" ">
                    <a-text value="space" text="align:center;width:0.8"></a-text>
                </a-box>
                <a-box btn id="keyboardBtn-suppr" position="1.24 0.115 0.025" rotation="22.5 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.15" key-type="text" key-value="suppr">
                    <a-text value="<-" text="align:center;width:0.8"></a-text>
                </a-box>
                <a-box btn id="keyboardBtn-enter" position="1.24 0 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.1;width:0.15" key-type="text" key-value="enter">
                    <a-text value="Enter" text="align:center;width:0.8"></a-text>
                    <a-box position="0.018 -0.11 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.12;width:0.115"></a-box>
                </a-box>
        </a-entity>

        <!-- World -->
        <a-entity id="world">
            <!-- Desk -->
            <a-box id="desk" position="0 1 -0.7" rotation="-45 0 0" height="0.3" ; geometry="depth: 0.1; width: 1.6; height: 0.3" color="#003141">
                <a-cylinder position="0 -0.15 0" rotation="-90 -90 0" height="1.6" radius="0.05" color="#003141"></a-cylinder>
            </a-box>
            <a-light class="desklight" position="-0.6 0.932 -0.751" light="type:point;color:#b0c8ff;groundColor:#b0c8ff;intensity:0.2"></a-light>
            <a-light class="desklight" position="0 0.932 -0.751" light="type:point;color:#b0c8ff;groundColor:#b0c8ff;intensity:0.2"></a-light>
            <a-light class="desklight" position="0.6 0.932 -0.751" light="type:point;color:#b0c8ff;groundColor:#b0c8ff;intensity:0.2"></a-light>
            <a-box id="nav" position="-0.807 1.13 -0.683" material="color:#0f0f0f" geometry="depth:0.275;height:3;width:0.03">
                <a-box class="selected" btn="showSelector:#homeScreen;hideSelector:#searchScreen;selectable:true" id="homeBtn" position="0.03 0.7 0" rotation="0 90 0" opacity="0.3" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                    <a-text value="Home" text="width:0.8;align:center"></a-text>
                </a-box>
                <a-box btn="showSelector:#searchScreen;hideSelector:#homeScreen;selectable:true" id="searchBtn" position="0.03 0.5245117302101869 0" rotation="0 90 0" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.14;width:0.14">
                    <a-text value="Search" text="width:0.8;align:center"></a-text>
                </a-box>
                <a-cylinder btn="toggleVisibilitySelector:#videoInfo" id="infoBtn" position="0.03 0.35 0" rotation="90 90 0" opacity="0.1" material="transparent:true" geometry="height:0.03;radius:0.07">
                    <a-text value="Info" text="width:0.8;align:center" rotation="-90 0 0"></a-text>
                </a-cylinder>
                <a-cylinder btn="" id="keyboardToggle" position="0.03 0.18 0" rotation="90 90 0" opacity="0.1" material="transparent:true" geometry="height:0.03;radius:0.07" data-vivaldi-spatnav-clickable="1">
                    <a-text value="Keyboard" text="width:0.8;align:center" rotation="-90 0 0"></a-text>
                </a-cylinder>
            </a-box>
            <a-box id="videoInfo" position="-0.785 1.6 -0.124" geometry="depth:0.03;height:0.9;width:0.7" color="<?php echo($black); ?>" rotation="0 90 0" visible="false">
            </a-box>
            <!-- Display -->
            <a-box id="display" opacity="0" position="0 1.6 -1.423" geometry="depth:0.02;height:0.9;width:1.6" color="#003141">
                <a-entity id="homeScreen" visible="true">
                    <a-text position="0 0.39 0.062" value="PeerTube VR player" text="width:2;shader:msdf;wrapCount:90;baseline:top;align:center" font="assets/fonts/FreeSans/FreeSans.json" negate="false"></a-text>
                    <a-box class="videoHome" data-peertubevideo-id="" position="0 0.193 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                    <a-box class="videoHome" data-peertubevideo-id="" position="0 -0.045 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                    <a-box class="videoHome" data-peertubevideo-id="" position="0 -0.29 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                </a-entity>
                <a-entity id="searchScreen" visible="false">
                    <a-box position="-0.515 0.344 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.015;width:0.47">
                        <a-text id="searchInput" value="Type something to search..." text="width:0.8" position="-0.226 0.05 0"></a-text>
                    </a-box>
                    <a-box class="videoResult" data-peertubevideo-id="" position="0 0.193 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5" visible="false">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                    <a-box class="videoResult" data-peertubevideo-id="" position="0 -0.045 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5" visible="false">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                    <a-box class="videoResult" data-peertubevideo-id="" position="0 -0.29 0.062" opacity="0.1" material="transparent:true" geometry="depth:0.02;height:0.215;width:1.5" visible="false">
                        <a-image class="thumbnail" src="" position="-0.58 0 0" geometry="height:0.165;width:0.3"></a-image>
                        <a-box position="-0.58 -0.07 0" opacity="0.1" material="opacity:0.2;transparent:true" color="<?php echo($black); ?>" geometry="depth:0.01;height:0.03;width:0.3">
                            <a-text class="duration" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top;align:right" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="0.145 0.0075 0"></a-text>
                            <a-text class="views" value="" text="width:0.9;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.145 0.0075 0"></a-text>
                        </a-box>
                        <a-text class="title" value="" text="width:1.41;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.079 0"></a-text>
                        <a-text class="user" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top;color:#bbbbbb" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.05 0"></a-text>
                        <a-text class="description" value="" text="width:1.15;shader:msdf;wrapCount:90;baseline:top" font="assets/fonts/FreeSans/FreeSans.json" negate="false" position="-0.414 0.011 0"></a-text>
                    </a-box>
                </a-entity>
            </a-box>
            <!-- Room -->
            <a-box class="sol" geometry="width:5;depth:4;height:0.1" color="<?php echo($grey); ?>" position="1.675 0 0.48"></a-box>
            <a-box class="plafond" geometry="width:5;depth:4;height:0.1" color="<?php echo($grey); ?>" position="1.675 2.36 0.48"></a-box>
            <a-box class="wall" geometry="width:0.1;depth:4;height:3" color="<?php echo($grey); ?>" position="-0.87 1 0.48"></a-box>
            <a-box class="wall" geometry="width:0.1;depth:4;height:3" color="<?php echo($grey); ?>" position="4.2 1 0.48"></a-box>
            <a-box geometry="width:5.14;depth:0.5;height:0.5" color="<?php echo($grey); ?>" position="1.036 2.4473 -1.409" rotation="45 0 0"></a-box>         
            <a-box geometry="width:5.61;depth:0.88;height:1.02" color="<?php echo($grey); ?>" position="1.788 2.4473 2.542" rotation="45 0 0"></a-box>
            <a-box geometry="width:5.61;depth:0.88;height:0.4" color="<?php echo($grey); ?>" position="1.788 -0.194 2.680" rotation="45 0 0"></a-box>
            <!-- Fenetres -->
            <a-entity class="fenetre" obj-model="obj:assets/3d/fenetre.obj" material="color:<?php echo($grey); ?>" scale="0.11 0.11 0.11" position="0 1.6 -1.52">
                <a-box position="0 0 0.5" geometry="depth:0.1;width:16;height:9" opacity="0.1" color="#fff"></a-box>
                <a-box position="0 9.503 0.5" geometry="depth:1;width:16;height:10" color="<?php echo($grey); ?>"></a-box>
                <a-box position="0 -9.503 0.5" geometry="depth:1;width:16;height:10" color="<?php echo($grey); ?>"></a-box>
            </a-entity>
            <a-entity class="fenetre" obj-model="obj:assets/3d/fenetre.obj" material="color:<?php echo($grey); ?>" scale="0.09 0.11 0.11" position="1.617 1.6 -1.52">
                <a-box position="0 0 0.5" geometry="depth:0.1;width:16;height:9" opacity="0.1" color="#fff"></a-box>
                <a-box position="0 9.503 0.5" geometry="width:16;height:10" color="<?php echo($grey); ?>"></a-box>
                <a-box position="0 -9.503 0.5" geometry="width:16;height:10" color="<?php echo($grey); ?>"></a-box>
            </a-entity>
            <a-entity class="fenetre" obj-model="obj:assets/3d/fenetre.obj" material="color:<?php echo($grey); ?>" scale="0.11 0.11 0.11" position="1.392 1.6 -0.724" rotation="0 -60 0">
                <a-box position="0 0 0.5" geometry="depth:0.1;width:16;height:9" opacity="0.8" color="#00318d"></a-box>
                <a-box position="0 9.503 0.5" geometry="depth:1;width:16;height:10" color="<?php echo($grey); ?>"></a-box>
                <a-box position="0 -9.503 0.5" geometry="depth:1;width:16;height:10" color="<?php echo($grey); ?>"></a-box>
            </a-entity>
            <a-box class="wall" geometry="height:2.75;width:0.6;depth:0.05" position="2.035 1.269 0.061" color="<?php echo($grey); ?>"></a-box>
            <a-box class="wall" geometry="height:2.73;width:2.05;depth:1.56" position="3.356 1.269 -0.726" color="<?php echo($grey); ?>"></a-box>
            <a-light position="2.974 1.772 0.498" light="type:point;color:#b0c8ff;groundColor:#b0c8ff;intensity:0.2"></a-light>
            
            <!-- Bed -->
            <a-entity class="bed" obj-model="obj:assets/3d/fenetre.obj" material="color:<?php echo($grey); ?>" scale="0.11 0.11 0.11" position="2.971 0.913 2.477">
                <a-box position="0 9.503 0.5" geometry="width:16;height:10" color="<?php echo($grey); ?>"></a-box>
                <a-box position="0 -9.503 0.5" geometry="width:16;height:10" color="<?php echo($grey); ?>"></a-box>
                <a-box position="9.616 3.275 0.5" geometry="width:3.27;height:25" color="<?php echo($grey); ?>"></a-box>
                <a-box position="-9.616 3.275 0.5" geometry="width:3.27;height:25" color="<?php echo($grey); ?>"></a-box>
            </a-entity>
            <a-box geometry="width:2.41;height:0.1" width="0" color="<?php echo($black); ?>" position="3.041 1.518 3.069"></a-box>
            <a-box geometry="width:2.41;height:0.1" width="0" color="<?php echo($black); ?>" position="3.041 0.33 3.069"></a-box>
            <a-box geometry="width:2;height:0.1" width="0" color="<?php echo($black); ?>" position="1.925 1.1 3.069" rotation="0 0 90"></a-box>            
            <a-box geometry="width:2;height:0.1" width="0" color="<?php echo($black); ?>" position="4.179 1.1 3.069" rotation="0 0 90"></a-box>
            <a-box geometry="width:2.36;height:0.2;depth:1.27" width="0" color="<?php echo($black); ?>" position="3.075 0.965 3.535" rotation="90 90 90"></a-box>
            <a-entity class="pillow" gltf-model="assets/3d/pillow_CC-BY-Google/pillow.gltf" scale="0.11 0.11 0.11" position="3.558 0.444 3.038" rotation="0 80 0"></a-entity>
            <a-light position="3.2 1.027 2.767" light="type:point;color:#ff6a4f;groundColor:#ff6a4f;intensity:0.2"></a-light>
            
            <!-- Door -->
            <a-entity class="door" obj-model="obj:assets/3d/porte.obj" material="color:<?php echo($black); ?>" scale="0.11 0.11 0.11" position="4.099 1.094 1.0887" rotation="0 90 90">
                <a-box position="0 3.149 -0.251" geometry="height:0.23;width:1.4" color="<?php echo($grey); ?>"></a-box>
            </a-entity>
            
            <!-- other -->
            <a-entity class="carpet" obj-model="obj:assets/3d/porte.obj" material="color:<?php echo($black); ?>" scale="0.19 0.14 0.11" position="1.988 0.027 1.089" rotation="-90 0 0"></a-entity>
            <a-entity class="light" obj-model="obj:assets/3d/porte.obj" material="color:#ffffff; emissive:#ffffff;emissiveIntensity:1" scale="0.19 0.14 0.05" position="1.988 2.302 1.089" rotation="-90 0 0"></a-entity>
            <a-entity class="light" obj-model="obj:assets/3d/porte.obj" material="color:<?php echo($grey); ?>" scale="0.19 0.14 0.05" position="1.988 2.253 1.089" rotation="-90 0 0"></a-entity>
            
            <a-box class="wardrobe" geometry="height:2.73;width:1.9;depth:0.35" position="0.783 1.14 2.689" color="<?php echo($black); ?>" material=""></a-box>
            <a-box class="wardrobe" geometry="height:2.73;width:0.05;depth:0.35" position="0.783 1.14 2.654" color="<?php echo($black); ?>" material=""></a-box>
            <a-box class="wall" geometry="height:2.73;width:0.679;depth:0.35" position="-0.508 1.14 2.65" color="<?php echo($grey); ?>" material=""></a-box>
            
            <a-box class="endOfDesk" position="1.15 1 -0.7" rotation="-45 0 0" height="0.3" geometry="depth:0.1;width:0.7" color="<?php echo($black); ?>">
                <a-cylinder position="0 -0.15 0" rotation="-90 -90 0" height="1.6" radius="0.05" color="<?php echo($black); ?>" geometry="height:0.7"></a-cylinder>
            </a-box>
            <a-box geometry="width:0.5;depth:0.7;height:1.1" color="<?php echo($black); ?>" position="1.05 0.59115 -1.12"></a-box>
            <a-box geometry="width:0.67;depth:0.33" color="<?php echo($black); ?>" position="1.13614 0.3897 -0.7105"></a-box>
            <a-entity class="desklamp" gltf-model="assets/3d/desklamp_CC-BY-Pookage-Hayes/model.gltf" position="0.913 0.4 -0.9" rotation="0 140 0" scale="0.4 0.4 0.4"></a-entity>
            <a-entity class="mug" obj-model="obj:assets/3d/mug_CC-BY-Google/model.obj" position="1.044 1.140 -0.889" rotation="-90 -140 0" scale="0.0005 0.0005 0.0005" material="color:#007c64"></a-entity>

            <a-entity class="books" gltf-model="assets/3d/books_CC-BY-Danny-Bittman/model.gltf" position="2.404 -0.215 1.954" rotation="0 60 0" scale="0.4 0.4 0.4"></a-entity>
            <a-entity class="camera" gltf-model="assets/3d/camera_CC-BY-Google/Camera.gltf" position="2.053 0.275 2.055" rotation="0 -60 0" scale="0.03 0.03 0.03"></a-entity>

            <a-entity class="plant" gltf-model="assets/3d/plant_CC-BY-Google/Houseplant.gltf" position="-0.466 0.0515 2.219" rotation="0 10 0" scale="0.15 0.15 0.15"></a-entity>

            <a-light class="desklamplight" position="0.824 1.427 -0.821" light="angle:80;type:spot;color:#fff3b0;groundColor:#fff3b0;distance:1" rotation="-90 0 0"></a-light>
            <a-light class="desklamplight" position="0.78 1.425 -0.821" light="type:point;color:#fff3b0;groundColor:#fff3b0;intensity:0.2" rotation="-90 0 0"></a-light>
            <a-light class="desklamplight" position="0.848 1.425 -0.777" light="type:point;color:#fff3b0;groundColor:#fff3b0;intensity:0.2" rotation="-90 0 0"></a-light>
            <a-light class="desklamplight" position="0.78 1.405 -0.751" light="type:point;color:#fff3b0;groundColor:#fff3b0;intensity:0.2" rotation="-90 0 0"></a-light>
        </a-entity>

        <!--a-sky src="#background" rotation="0 -90 0"></a-sky-->
        <a-sky color="#050014"></a-sky>
    </a-scene>
</body>

</html>