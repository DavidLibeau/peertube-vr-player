difference() {
    translate([-8,-4.5,0]) cube([16,9,1]);
    hull(){
        cylindreX = 5.5;
        cylindreY = 2;
        cylindreRadius = 1.8;
        translate([cylindreX,cylindreY,0]) cylinder(h=4, r=cylindreRadius, center=true, $fn=100);
        translate([cylindreX,-cylindreY,0]) cylinder(h=4, r=cylindreRadius, center=true, $fn=100);
        translate([-cylindreX,cylindreY,0]) cylinder(h=4, r=cylindreRadius, center=true, $fn=100);
        translate([-cylindreX,-cylindreY,0]) cylinder(h=4, r=cylindreRadius, center=true, $fn=100);
    }
}