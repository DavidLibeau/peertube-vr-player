/******/
(function (modules) { // webpackBootstrap
    /******/ // The module cache
    /******/
    var installedModules = {};

    /******/ // The require function
    /******/
    function __webpack_require__(moduleId) {

        /******/ // Check if module is in cache
        /******/
        if (installedModules[moduleId])
            /******/
            return installedModules[moduleId].exports;

        /******/ // Create a new module (and put it into the cache)
        /******/
        var module = installedModules[moduleId] = {
            /******/
            exports: {},
            /******/
            id: moduleId,
            /******/
            loaded: false
            /******/
        };

        /******/ // Execute the module function
        /******/
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

        /******/ // Flag the module as loaded
        /******/
        module.loaded = true;

        /******/ // Return the exports of the module
        /******/
        return module.exports;
        /******/
    }


    /******/ // expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;

    /******/ // expose the module cache
    /******/
    __webpack_require__.c = installedModules;

    /******/ // __webpack_public_path__
    /******/
    __webpack_require__.p = "";

    /******/ // Load entry module and return exports
    /******/
    return __webpack_require__(0);
    /******/
})
/************************************************************************/
/******/
([
/* 0 */
/***/
    function (module, exports) {

        if (typeof AFRAME === "undefined") {
            throw new Error('Component attempted to register before AFRAME was available.');
        }

        /**
         ** Video control component for A-Frame.
         */

        AFRAME.registerComponent('video-controls', {
            schema: {
                src: {
                    type: "string"
                },
                timecode: {
                    type: "number",
                    default: 0
                },
                size: {
                    type: "number",
                    default: 1.0
                },
                distance: {
                    type: "number",
                    default: 2.0
                },
                quality: {
                    type: "string",
                    default: "auto"
                },
                state: {
                    type: "string",
                    default: "undefined"
                }
                /*backgroundColor: {
                    default: 'black'
                },
                barColor: {
                    default: 'red'
                },
                textColor: {
                    default: 'yellow'
                },
                infoTextBottom: {
                    default: DEFAULT_INFO_TEXT_BOTTOM
                },
                infoTextTop: {
                    default: DEFAULT_INFO_TEXT_TOP
                },
                infoTextFont: {
                    default: '35px Helvetica Neue'
                },
                statusTextFont: {
                    default: '30px Helvetica Neue'
                },
                timeTextFont: {
                    default: '70px Helvetica Neue'
                }*/
            },

            position_time_from_steps: function () {

                var unit_offset = this.current_step / this.bar_steps;

                if (this.video_el.readyState > 0) {

                    this.video_el.currentTime = unit_offset * this.video_el.duration;
                }


            },

            // Puts the control in from of the camera, at this.data.distance, facing it...

            position_control_from_camera: function () {
                /*
                var self = this;

                var camera = self.el.sceneEl.camera;

                if (camera) {

                    var camera_rotation = camera.el.getAttribute("rotation");

                    var camera_yaw = camera_rotation.y;

                    // Set position of menu based on camera yaw and data.pitch

                    // Have to add 1.6m to camera.position.y (????)

                    self.y_position = camera.position.y + 1.6;
                    self.x_position = -self.data.distance * Math.sin(camera_yaw * Math.PI / 180.0);
                    self.z_position = -self.data.distance * Math.cos(camera_yaw * Math.PI / 180.0);

                    self.el.setAttribute("position", [self.x_position, self.y_position, self.z_position].join(" "));

                    // and now, make our controls rotate towards origin

                    this.el.object3D.lookAt(new THREE.Vector3(camera.position.x, camera.position.y + 1.6, camera.position.z));

                }
                */
                self.y_position = 0;
                self.x_position = 0;
                self.z_position = 0;

            },
            /**
             * Called once when component is attached. Generally for initial setup.
             */
            init: function () {
                console.log("init");
                var self = this;


                this.bar_steps = 10.0;

                this.current_step = 0.0;

                this.el.setAttribute("visible", true);

                this.video_selector = this.data.src;

                this.video_el = document.querySelector(this.video_selector);

                this.video_id=undefined;
                
                this.state = "paused";

                // Change source event
                this.el.addEventListener("changeSource", function () {
                    self.video_el.currentTime = self.data.timecode;
                    self.el.querySelector(".qualityBtn>a-text").setAttribute("value", self.data.quality);
                    console.log(self.state);
                    if (self.state == "playing") {
                        self.video_el.play();
                    }
                });

                // Change icon to 'play' on end

                this.video_el.addEventListener("ended", function () {
                    self.state = "ended";
                    document.querySelector("#videoControls .playBtn>a-image").setAttribute("src", "#play-image");
                });

                // Change icon to 'pause' on start.

                this.video_el.addEventListener("pause", function () {
                    self.state = "paused";
                    document.querySelector("#videoControls .playBtn>a-image").setAttribute("src", "#play-image");
                });

                // Change icon to 'play' on pause.

                this.video_el.addEventListener("playing", function () {
                    self.state = "playing";
                    document.querySelector("#videoControls .playBtn>a-image").setAttribute("src", "#pause-image");
                });


                // Btn
                this.btns = document.querySelectorAll("#videoControls [btn]");
                for (var i = 0; i < this.btns.length; i++) {
                    this.btns[i].addEventListener("mouseup", function (event) {
                        // Hide timer
                        self.hideTime = 10;
                        // Btn
                        if (document.getElementById("video").getAttribute("src") != undefined && document.getElementById("video").getAttribute("src") != "") {
                            if (this.classList.contains("playBtn")) {
                                if (!self.video_el.paused) {
                                    this.children[0].setAttribute("src", "#play-image");
                                    self.video_el.pause();
                                } else {
                                    this.children[0].setAttribute("src", "#pause-image");
                                    self.video_el.play();
                                }
                            } else if (this.classList.contains("beginningBtn")) {
                                self.current_step = 0.0;
                                self.position_time_from_steps();
                            } else if (this.classList.contains("backwardBtn")) {
                                self.current_step = self.current_step > 0 ? self.current_step - 1 : self.current_step;
                                self.position_time_from_steps();
                            } else if (this.classList.contains("forwardBtn")) {
                                self.current_step = self.current_step < (self.bar_steps) ? self.current_step + 1 : self.current_step;
                                self.position_time_from_steps();
                            } else if (this.classList.contains("volumeBtn")) {
                                self.video_el.muted = !self.video_el.muted;
                                if (!self.video_el.muted) {
                                    this.children[0].setAttribute("src", "#volumeOn-image");
                                } else {
                                    this.children[0].setAttribute("src", "#volumeOff-image");
                                }
                            } else if (this.classList.contains("qualityBtn")) {
                                for (var j = 0; j < this.children.length; j++) {
                                    if (this.children[j].classList.contains("btnMenu")) {
                                        this.children[j].setAttribute("visible", !this.children[j].getAttribute("visible"));
                                    }
                                }
                            } else if (this.classList.contains("changeQualityBtn")) {
                                loadVideo(self.video_id, this.children[0].getAttribute("value"), self.video_el.currentTime);
                                document.querySelector("[video-controls] .qualityBtn .btnMenu").setAttribute("visible", "false");
                            } else if (this.classList.contains("fullscreenBtn")) {
                                setVisible(document.getElementById("world"), !document.getElementById("world").getAttribute("visible"));
                                self.hideTime = 5;
                                if (self.hideTimer) {
                                    clearInterval(self.hideTimer);
                                    delete self.hideTimer;
                                } else {
                                    self.hideTimer = setInterval(function () {
                                        var raycaster = document.querySelector("[raycaster],[cursor]").components.raycaster.raycaster;
                                        if (!raycaster.intersectObject(self.el.object3D, true).length == 0) { //hover video-control
                                            self.hideTime = 5;
                                        } else { //not hovering
                                            if (self.hideTime == 0) {
                                                self.el.setAttribute("visible", false);
                                            } else {
                                                self.hideTime--;
                                            }
                                        }
                                    }, 1000);
                                }
                            }
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    });
                }

                // Fullscreen off
                this.el.sceneEl.addEventListener("loaded", function () {
                    self.el.sceneEl.addEventListener("click", function () {
                        var raycaster = document.querySelector("[raycaster],[cursor]").components.raycaster.raycaster;
                        //console.log(raycaster.intersectObject(self.el.object3D, true).length);
                        if (raycaster.intersectObject(self.el.object3D, true).length == 0) {
                            self.hideTime = 5;
                            self.el.setAttribute("visible", true);
                        }
                    });
                });

                //Keyboard management
                window.addEventListener('keyup', function (event) {
                    switch (event.keyCode) {

                        // If space bar is pressed, fire click on play_image
                        case 32:
                            if (self.video_el.paused) {
                                self.video_el.play();
                            } else {
                                self.video_el.pause();
                            }
                            break;

                            // Arrow left: beginning
                        case 37:
                            self.current_step = 0.0;
                            self.position_time_from_steps();
                            break;

                            // Arrow right: end
                        case 39:
                            self.current_step = self.bar_steps;
                            self.position_time_from_steps();

                            break;

                            // Arrow up: one step forward
                        case 38:
                            self.current_step = self.current_step < (self.bar_steps) ? self.current_step + 1 : self.current_step;
                            self.position_time_from_steps();
                            break;

                            // Arrow down: one step back
                        case 40:
                            self.current_step = self.current_step > 0 ? self.current_step - 1 : self.current_step;
                            self.position_time_from_steps();
                            break;

                    }
                }, false);


                // Attach double click behavior outside player once scene is loaded

                /*this.el.sceneEl.addEventListener("loaded", function () {

                    self.position_control_from_camera();

                    this.addEventListener("dblclick", function () {

                        var raycaster = document.querySelector("[raycaster],[cursor]").components.raycaster.raycaster;

                        // Double click is outside the player
                        // (note that for some reason you cannot prevent a dblclick on player from bubbling up (??)

                        if (raycaster.intersectObject(self.el.object3D, true).length == 0) {

                            // If controls are show: hide


                            if (self.el.getAttribute("visible")) {
                                self.el.setAttribute("visible", false);
                            }
                            // Else, show at 'distance' from camera
                            else {
                                self.el.setAttribute("visible", true);

                                self.position_control_from_camera();
                            }
                        }

                    });


                });*/

            },

            /**
             * Called when component is attached and when component data changes.
             * Generally modifies the entity based on the data.
             */
            update: function (oldData) {

                this.position_control_from_camera();

            },

            /**
             * Called when a component is removed (e.g., via removeAttribute).
             * Generally undoes all modifications to the entity.
             */
            remove: function () {},

            /**
             * Called on each scene tick.
             */
            tick: function (t) {

                // Refresh every 250 millis

                if (typeof (this.last_time) === "undefined" || (t - this.last_time) > 250) {

                    // At the very least, have all video metadata
                    // (https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/readyState)

                    if (this.video_el.readyState > 0) {

                        // Get current position minutes and second, and add leading zeroes if needed

                        var current_minutes = Math.floor(this.video_el.currentTime / 60);
                        var current_seconds = Math.floor(this.video_el.currentTime % 60);


                        current_minutes = current_minutes < 10 ? "0" + current_minutes : current_minutes;
                        current_seconds = current_seconds < 10 ? "0" + current_seconds : current_seconds;

                        // Get video duration in  minutes and second, and add leading zeroes if needed

                        var duration_minutes = Math.floor(this.video_el.duration / 60);
                        var duration_seconds = Math.floor(this.video_el.duration % 60);


                        duration_minutes = duration_minutes < 10 ? "0" + duration_minutes : duration_minutes;
                        duration_seconds = duration_seconds < 10 ? "0" + duration_seconds : duration_seconds;

                        // Refresh time information : currentTime / duration

                        var time_info_text = current_minutes + ":" + current_seconds + " / " + duration_minutes + ":" + duration_seconds;

                        //  display buffered TimeRanges

                        if (this.video_el.buffered.length > 0) {

                            // Synchronize current step with currentTime

                            this.current_step = Math.round((this.video_el.currentTime / this.video_el.duration) * this.bar_steps);


                            // If seeking to position, show

                            if (this.video_el.seeking) {
                                /*ctx.font = this.data.statusTextFont;
                                ctx.fillStyle = this.data.textColor;
                                ctx.textAlign = "end";
                                ctx.fillText("Seeking", this.bar_canvas.width * 0.95, this.bar_canvas.height * 0.60);*/
                            } else {

                                var percent = (this.video_el.buffered.end(this.video_el.buffered.length - 1) / this.video_el.duration) * 100;

                                /*ctx.font = this.data.statusTextFont;
                                ctx.fillStyle = this.data.textColor;
                                ctx.textAlign = "end";

                                ctx.fillText(percent.toFixed(0) + "% loaded", this.bar_canvas.width * 0.95, this.bar_canvas.height * 0.60);*/
                            }


                            //Times
                            this.timeTotalEl = document.querySelector("[video-controls] .timeTotal");
                            this.timePlayedEl = document.querySelector("[video-controls] .timePlayed");

                            this.timeTotalEl.setAttribute("value", duration_minutes + ":" + duration_seconds);
                            this.timePlayedEl.setAttribute("value", current_minutes + ":" + current_seconds);
                            //current_minutes + ":" + current_seconds + " / " + duration_minutes + ":" + duration_seconds

                            //ProgressBar
                            this.progressBarEl = document.querySelector("[video-controls] .progressBar");


                            // Played bar

                            this.progressBarPlayedEl = document.querySelector("[video-controls] .progressBar>a-box:nth-child(1)");
                            //width
                            this.progressBarPlayedEl.setAttribute("width", 0.001 + (this.video_el.currentTime / this.video_el.duration) * this.progressBarEl.getAttribute("width"));
                            //pos x
                            if (this.progressBarPlayedEl.getAttribute("width") == 0) {
                                this.progressBarPlayedEl.setAttribute("width", 0.001);
                                this.progressBarPlayedEl.object3D.position.set(-this.progressBarEl.getAttribute("width") / 2, this.progressBarPlayedEl.object3D.position.y, this.progressBarPlayedEl.object3D.position.z);
                            } else {
                                this.progressBarPlayedEl.object3D.position.set(-this.progressBarEl.getAttribute("width") / 2 + this.progressBarPlayedEl.getAttribute("width") / 2, this.progressBarPlayedEl.object3D.position.y, this.progressBarPlayedEl.object3D.position.z);
                            }

                            // Buffered bar

                            this.progressBarBufferedEl = document.querySelector("[video-controls] .progressBar>:nth-child(2)");

                            for (var i = 0; i < this.video_el.buffered.length; i++) {
                                //width
                                this.progressBarBufferedEl.setAttribute("width", 0.001 + ((this.video_el.buffered.end(i) / this.video_el.duration) * this.progressBarEl.getAttribute("width")) - this.progressBarPlayedEl.getAttribute("width"));
                                //pos x
                                if (this.progressBarBufferedEl.getAttribute("width") == 0) {
                                    this.progressBarBufferedEl.setAttribute("width", 0.001);
                                    this.progressBarBufferedEl.object3D.position.set(-this.progressBarEl.getAttribute("width") / 2, this.progressBarBufferedEl.object3D.position.y, this.progressBarBufferedEl.object3D.position.z);
                                } else {
                                    this.progressBarBufferedEl.object3D.position.set((-this.progressBarEl.getAttribute("width") / 2 + this.progressBarBufferedEl.getAttribute("width") / 2) + parseFloat(this.progressBarPlayedEl.getAttribute("width")), this.progressBarBufferedEl.object3D.position.y, this.progressBarBufferedEl.object3D.position.z);
                                }

                            }

                            // Background bar

                            this.progressBarBackgroundEl = document.querySelector("[video-controls] .progressBar>:nth-child(3)");
                            //width
                            var progressBarBackgroundWidth = (this.progressBarEl.getAttribute("width") - (parseFloat(this.progressBarPlayedEl.getAttribute("width")) + parseFloat(this.progressBarBufferedEl.getAttribute("width"))));
                            //console.log(progressBarBackgroundWidth);
                            this.progressBarBackgroundEl.setAttribute("width", progressBarBackgroundWidth);
                            //pos x
                            if (this.progressBarBufferedEl.getAttribute("width") == 0) {
                                this.progressBarBufferedEl.setAttribute("width", 0.001);
                                this.progressBarBackgroundEl.object3D.position.set(-this.progressBarEl.getAttribute("width") / 2, this.progressBarBackgroundEl.object3D.position.y, this.progressBarBackgroundEl.object3D.position.z);
                            } else {
                                this.progressBarBackgroundEl.object3D.position.set((-this.progressBarEl.getAttribute("width") / 2 + this.progressBarBackgroundEl.getAttribute("width") / 2) + parseFloat(this.progressBarPlayedEl.getAttribute("width")) + parseFloat(this.progressBarBufferedEl.getAttribute("width")), this.progressBarBackgroundEl.object3D.position.y, this.progressBarBackgroundEl.object3D.position.z);
                            }


                        }


                    }

                    // Save this 't' to last_time

                    this.last_time = t;
                }
            },

            /**
             * Called when entity pauses.
             * Use to stop or remove any dynamic or background behavior such as events.
             */
            pause: function () {},

            /**
             * Called when entity resumes.
             * Use to continue or add any dynamic or background behavior such as events.
             */
            play: function () {}
        });


        /***/
    }
/******/
    ]);