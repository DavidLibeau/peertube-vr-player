/******/
(function (modules) { // webpackBootstrap
    /******/ // The module cache
    /******/
    var installedModules = {};

    /******/ // The require function
    /******/
    function __webpack_require__(moduleId) {

        /******/ // Check if module is in cache
        /******/
        if (installedModules[moduleId])
            /******/
            return installedModules[moduleId].exports;

        /******/ // Create a new module (and put it into the cache)
        /******/
        var module = installedModules[moduleId] = {
            /******/
            exports: {},
            /******/
            id: moduleId,
            /******/
            loaded: false
            /******/
        };

        /******/ // Execute the module function
        /******/
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

        /******/ // Flag the module as loaded
        /******/
        module.loaded = true;

        /******/ // Return the exports of the module
        /******/
        return module.exports;
        /******/
    }


    /******/ // expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;

    /******/ // expose the module cache
    /******/
    __webpack_require__.c = installedModules;

    /******/ // __webpack_public_path__
    /******/
    __webpack_require__.p = "";

    /******/ // Load entry module and return exports
    /******/
    return __webpack_require__(0);
    /******/
})
/************************************************************************/
/******/
([
/* 0 */
/***/
    function (module, exports) {

        if (typeof AFRAME === "undefined") {
            throw new Error('Component attempted to register before AFRAME was available.');
        }

        /**
         ** Video control component for A-Frame.
         */

        AFRAME.registerComponent("btn", {
            schema: {
                showSelector: {
                    type: "string",
                    default: "undefined"
                },
                hideSelector: {
                    type: "string",
                    default: "undefined"
                },
                toggleVisibilitySelector: {
                    type: "string",
                    default: "undefined"
                },
                selectable: {
                    type: "boolean",
                    default: false
                },
            },

            /**
             * Called once when component is attached. Generally for initial setup.
             */
            init: function () {
                var self = this;
                // Btn
                this.el.addEventListener("mouseenter", function (event) {
                    this.setAttribute("opacity", "0.2");
                });
                this.el.addEventListener("mouseleave", function (event) {
                    if (this.classList.contains("selected")) {
                        this.setAttribute("opacity", "0.3");
                    } else {
                        this.setAttribute("opacity", "0.1");
                    }
                });
                this.el.addEventListener("mousedown", function (event) {
                    if (this.getAttribute("visible") != false) {
                        this.setAttribute("opacity", "0.3");
                        if (this.nodeName == "A-BOX") {
                            this.children[0].object3D.position.set(0, 0, -0.01);
                        } else if (this.nodeName == "A-CYLINDER") {
                            this.children[0].object3D.position.set(0, -0.01, 0);
                        }

                        if (self.data.showSelector != "undefined") {
                            var showEl = document.querySelectorAll(self.data.showSelector);
                            for (var i = 0; i < showEl.length; i++) {
                                showEl[i].setAttribute("visible", "true");
                            }
                        }
                        if (self.data.hideSelector != "undefined") {
                            var hideEl = document.querySelectorAll(self.data.hideSelector);
                            for (var i = 0; i < hideEl.length; i++) {
                                hideEl[i].setAttribute("visible", "false");
                            }
                        }
                        if (self.data.toggleVisibilitySelector != "undefined") {
                            var toggleEl = document.querySelectorAll(self.data.toggleVisibilitySelector);
                            for (var i = 0; i < toggleEl.length; i++) {
                                toggleEl[i].setAttribute("visible", !toggleEl[i].getAttribute("visible"));
                            }
                        }
                        if (self.data.selectable) {
                            var selectEl = document.querySelectorAll(".selected");
                            for (var i = 0; i < selectEl.length; i++) {
                                selectEl[i].classList.remove("selected");
                                selectEl[i].setAttribute("opacity", "0.1");
                            }
                            self.el.classList.add("selected");
                        }
                    }
                });
                this.el.addEventListener("mouseup", function (event) {
                    if (this.getAttribute("visible") != false) {

                        if (this.classList.contains("selected")) {
                            this.setAttribute("opacity", "0.3");
                        } else {
                            this.setAttribute("opacity", "0.1");
                        }
                        this.children[0].object3D.position.set(0, 0, 0);
                    }
                });

                if (this.el.classList.contains("selected")) {
                    this.el.setAttribute("opacity", "0.3");
                }
            },

            /**
             * Called when component is attached and when component data changes.
             * Generally modifies the entity based on the data.
             */
            update: function (oldData) {},

            /**
             * Called when a component is removed (e.g., via removeAttribute).
             * Generally undoes all modifications to the entity.
             */
            remove: function () {},

            /**
             * Called on each scene tick.
             */
            tick: function (t) {},
        });


        /***/
    }
/******/
    ]);